﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Security;

using BlogEngine.Core.Data.Contracts;
using BlogEngine.Core.Data.Models;
using BlogEngine.Core.FileSystem;
using BlogEngine.Core.Providers;

#endregion

namespace BlogEngine.Core.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FileManagerRepository : IFileManagerRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <param name="path"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<FileInstance> Find(int take = 10, int skip = 0, string path = "", string order = "")
        {
            var rwr = Utils.RelativeWebRoot;
            var responsePath = "root";
            List<FileInstance> list = new List<FileInstance>();
            var directory = BlogService.GetDirectory(path);
            if (!directory.IsRoot)
            {
                list.Add(new FileInstance()
                {
                    FileSize = "",
                    FileType = FileType.Directory,
                    Created = "",
                    FullPath = directory.Parent.FullPath,
                    Name = "..."
                });
                responsePath = "root" + directory.FullPath;
            }

            foreach (var dir in directory.Directories)
                list.Add(new FileInstance()
                {
                    FileSize = "",
                    FileType = FileType.Directory,
                    Created = dir.DateCreated.ToString(),
                    FullPath = dir.FullPath,
                    Name = dir.Name.Replace("/", "")
                });


            foreach (var file in directory.Files)
                list.Add(new FileInstance()
                {
                    FileSize = file.FileSizeFormat,
                    Created = file.DateCreated.ToString(),
                    FileType = file.IsImage ? FileType.Image : FileType.File,
                    FullPath = file.FilePath,
                    Name = file.Name
                });

            return list;
        }
    }
}
