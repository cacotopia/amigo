﻿using BlogEngine.Core.Data.Models;
using BlogEngine.Core.FileSystem;
using System;
using System.Collections.Generic;

namespace BlogEngine.Core.Data.Contracts
{
    public interface IFileManagerRepository
    {
        /// <summary>
        /// Get list of fileinstance
        /// </summary>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <param name="path"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        IEnumerable<FileInstance> Find(int take = 10, int skip = 0, string path = "", string order = "");
    }
}
