﻿namespace BlogEngine.Core.Data.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Stats
    {
        /// <summary>
        /// 
        /// </summary>
        public int PublishedPostsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DraftPostsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PublishedPagesCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DraftPagesCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PublishedCommentsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UnapprovedCommentsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SpamCommentsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CategoriesCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TagsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int UsersCount { get; set; }
    }
}
