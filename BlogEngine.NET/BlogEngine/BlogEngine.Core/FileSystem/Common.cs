﻿using System.Collections.Generic;

namespace BlogEngine.Core.FileSystem
{
    /// <summary>
    /// 
    /// </summary>
    public class FileResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public FileResponse()
        {
            Files = new List<FileInstance>();
            Path = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<FileInstance> Files { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FileInstance
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Created { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public FileType FileType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullPath { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// 
        /// </summary>
        Directory,

        /// <summary>
        /// 
        /// </summary>
        File,

        /// <summary>
        /// 
        /// </summary>
        Image,

        /// <summary>
        /// 
        /// </summary>
        None
    }
}
