﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ALinq;
using ALinq.Mapping;
using ALinq.MySQL;

#endregion

namespace BlogEngine.Core.FileSystem
{
    [ALinq.Mapping.Database(Name = "BlogEngineFileStore")]
    public partial class MySqlFileStoreDB :ALinq.DataContext
    {
        //private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();

        private static ALinq.Mapping.MappingSource mappingSource = new ALinq.Mapping.AttributeMappingSource();
        
        #region Extensibility Method Definitions

        partial void OnCreated();
        partial void Insertbe_FileStoreDirectory(FileStoreDirectory instance);
        partial void Updatebe_FileStoreDirectory(FileStoreDirectory instance);
        partial void Deletebe_FileStoreDirectory(FileStoreDirectory instance);
        partial void Insertbe_FileStoreFileThumb(FileStoreFileThumb instance);
        partial void Updatebe_FileStoreFileThumb(FileStoreFileThumb instance);
        partial void Deletebe_FileStoreFileThumb(FileStoreFileThumb instance);
        partial void Insertbe_FileStoreFile(FileStoreFile instance);
        partial void Updatebe_FileStoreFile(FileStoreFile instance);
        partial void Deletebe_FileStoreFile(FileStoreFile instance);
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        public MySqlFileStoreDB(string connection) :
            base(connection, mappingSource)
        {
            OnCreated();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        public MySqlFileStoreDB(System.Data.IDbConnection connection) :
            base(connection, mappingSource)
        {
            OnCreated();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="mappingSource"></param>
        public MySqlFileStoreDB(string connection, ALinq.Mapping.MappingSource mappingSource) :
            base(connection, mappingSource)
        {
            OnCreated();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="mappingSource"></param>
        public MySqlFileStoreDB(System.Data.IDbConnection connection, ALinq.Mapping.MappingSource mappingSource) :
            base(connection, mappingSource)
        {
            OnCreated();
        }

        /// <summary>
        /// 
        /// </summary>
        public ALinq.Table<FileStoreDirectory> FileStoreDirectories
        {
            get
            {
                return this.GetTable<FileStoreDirectory>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ALinq.Table<FileStoreFileThumb> FileStoreFileThumbs
        {
            get
            {
                return this.GetTable<FileStoreFileThumb>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ALinq.Table<FileStoreFile> FileStoreFiles
        {
            get
            {
                return this.GetTable<FileStoreFile>();
            }
        }
    }
}
