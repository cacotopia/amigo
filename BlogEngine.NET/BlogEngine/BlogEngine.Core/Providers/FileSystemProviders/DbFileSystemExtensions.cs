﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion

namespace BlogEngine.Core.Providers
{
    #region Extension Methods
    /// <summary>
    /// static classes for the DbFileSystem
    /// </summary>
    public static class DbFileSystemExtensions
    {
        /// <summary>
        /// copy's the database directory object to a Directory object
        /// </summary>
        /// <param name="inObj">the database directory to copy</param>
        /// <returns>a new Directory object</returns>
        public static FileSystem.Directory CopyToDirectory(this FileSystem.FileStoreDirectory inObj)
        {
            if (inObj == null)
                return null;
            return new FileSystem.Directory()
            {
                DateCreated = inObj.CreateDate,
                DateModified = inObj.LastModify,
                FullPath = inObj.FullPath,
                Id = inObj.Id,
                LastAccessTime = inObj.LastAccess,
                Name = inObj.Name,
                IsRoot = inObj.ParentID == null,
            };
        }

        /// <summary>
        /// copys a database File object to a File object
        /// </summary>
        /// <param name="inObj">the database file object to copy</param>
        /// <returns>a new File object</returns>
        public static FileSystem.File CopyToFile(this FileSystem.FileStoreFile inObj)
        {
            if (inObj == null)
                return null;
            return new FileSystem.File()
            {
                DateCreated = inObj.CreateDate,
                DateModified = inObj.LastModify,
                FilePath = inObj.FullPath,
                FileSize = inObj.Size,
                Id = inObj.FileID.ToString(),
                LastAccessTime = inObj.LastAccess,
                Name = inObj.Name,
                FileContents = inObj.Contents.ToArray(),
                FullPath = inObj.FullPath
            };
        }

        /// <summary>
        /// removes the virtual path of BlogStorageLocation + files from the virtual path. 
        /// </summary>
        /// <param name="VirtualPath">the virtual path to replace against</param>
        /// <returns>the repleaced string</returns>
        public static string VirtualPathToDbPath(this string VirtualPath)
        {
            return VirtualPath.Replace(Blog.CurrentInstance.StorageLocation + Utils.FilesFolder, "");
        }
    }

    #endregion
}
