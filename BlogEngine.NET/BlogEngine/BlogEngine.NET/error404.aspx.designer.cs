﻿//------------------------------------------------------------------------------
// <自动生成>
//     此代码由工具生成。
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。 
// </自动生成>
//------------------------------------------------------------------------------



public partial class error404 {
    
    /// <summary>
    /// divSearchEngine 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSearchEngine;
    
    /// <summary>
    /// divSearchResult 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSearchResult;
    
    /// <summary>
    /// divExternalReferrer 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divExternalReferrer;
    
    /// <summary>
    /// divInternalReferrer 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divInternalReferrer;
    
    /// <summary>
    /// SearchBox2 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::App_Code.Controls.SearchBox SearchBox2;
    
    /// <summary>
    /// divDirectHit 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlGenericControl divDirectHit;
    
    /// <summary>
    /// phSearchResult 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder phSearchResult;
    
    /// <summary>
    /// SearchBox1 控件。
    /// </summary>
    /// <remarks>
    /// 自动生成的字段。
    /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
    /// </remarks>
    protected global::App_Code.Controls.SearchBox SearchBox1;
}
