<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="BlogEngine.Core.Web.Controls.PostViewBase" %>

<div class="post xfolkentry" id="post<%=Index %>">
  <h1><a href="<%=Post.RelativeLink %>" class="taggedlink"><%=Server.HtmlEncode(Post.Title) %></a></h1>
  <div>
  <span class="author">by <a href="<%=VirtualPathUtility.ToAbsolute("~/") + "author/" + BlogEngine.Core.Utils.RemoveIllegalCharacters(Post.Author) %>.aspx"><%=Post.AuthorProfile != null ? Post.AuthorProfile.DisplayName : Post.Author %></a></span>
  <span class="postMetaElementSeparator"> * </span>
  <span><%=Post.DateCreated.ToString("MMMM") %></span>
  <span><%=Post.DateCreated.ToString(" d") %></span><span class="postMetaElementSeparator"> * </span>
  <span class="categories"><%=CategoryLinks(" * ") %></span>
  </div>
  <br />
  <div class="text"><asp:PlaceHolder ID="BodyContent" runat="server" /></div>
  


  <div class="bottom">
    <%=Rating %>
    <br />
    <div class="tagBox">
    <span class="tags">tagz: <span style="color: #fedce5"><%=TagLinks(" || ") %></span></span>
    </div>
  </div>

  <div class="postFooter">    
    <div class="fb">
      <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" layout="button_count" show_faces="false" width="450" action="recommend" font="arial"></fb:like>
    </div>
    <div class="tw">
    <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    </div>
  </div>
  <div class="administrativeLinks">
    <%=AdminLinks %>
    
    <% if (BlogEngine.Core.BlogSettings.Instance.ModerationType == BlogEngine.Core.BlogSettings.Moderation.Disqus)
       { %>
    <a rel="nofollow" href="<%=Post.PermaLink %>#disqus_thread"><%=Resources.labels.comments %></a>
    <%}
       else
       { %>
    <a rel="bookmark" href="<%=Post.PermaLink %>" title="<%=Server.HtmlEncode(Post.Title) %>">Permalink</a><span style="color: #fde2ea"> |</span>
    <a rel="nofollow" href="<%=Post.RelativeLink %>#comment">Leave a Comment (<%=Post.ApprovedComments.Count %>)</a>   
    <%} %>
  </div>
</div>
