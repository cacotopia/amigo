<%@ Control Language="C#" EnableViewState="False" Inherits="BlogEngine.Core.Web.Controls.CommentViewBase" %>

<div id="id_<%=Comment.Id %>" class="vcard comment<%= Post.Author.Equals(Comment.Author, StringComparison.OrdinalIgnoreCase) ? " self" : "" %>">
  
   <div class="author"><%= Flag %> <%=Comment.Author%>:</div>
     <div class="gravatar"><%= Gravatar(80)%></div>
     
  <br />
   
   <p class="content"><%= Text %></p>
   
<p class="date"><%= Comment.DateCreated %> <a href="#id_<%=Comment.Id %>">#</a></p>
    
    <%= AdminLinks %>

</div>
