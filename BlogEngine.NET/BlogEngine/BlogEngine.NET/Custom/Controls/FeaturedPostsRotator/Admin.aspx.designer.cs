﻿//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。 
// </自动生成>
//------------------------------------------------------------------------------

    public partial class UserControlsFeaturedPostsAdmin
    {

        /// <summary>
        /// phComments 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.PlaceHolder phComments;

        /// <summary>
        /// phTrckbacks 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.PlaceHolder phTrckbacks;

        /// <summary>
        /// phAddComment 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.PlaceHolder phAddComment;

        /// <summary>
        /// phCommentForm 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.PlaceHolder phCommentForm;

        /// <summary>
        /// hiddenReplyTo 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.HiddenField hiddenReplyTo;

        /// <summary>
        /// simplecaptcha 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::App_Code.Controls.SimpleCaptchaControl simplecaptcha;

        /// <summary>
        /// recaptcha 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::App_Code.Controls.RecaptchaControl recaptcha;

        /// <summary>
        /// hfCaptcha 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.HiddenField hfCaptcha;

        /// <summary>
        /// lbCommentsDisabled 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        //protected global::System.Web.UI.WebControls.Label lbCommentsDisabled;

        protected global::System.Web.UI.WebControls.DropDownList ddPosts;

        protected global::System.Web.UI.WebControls.FileUpload txtUploadImage;

        protected global::System.Web.UI.WebControls.Button btnUploadImage;

        protected global::System.Web.UI.WebControls.GridView gridFeaturedPostImages;

        protected global::System.Web.UI.WebControls.Label lblUseJquery;

        protected global::System.Web.UI.WebControls.CheckBox chkUseJquery;

        protected global::System.Web.UI.WebControls.Button btnSaveSettings;


    }

